# Versions

Soquee Versions is an experimental Git client written in Rust and GTK3. It's
probably not far enough along to use for real projects, but it does manage its
own repo.

For information about submitting patches and contributing to the project, see
[`CONTRIBUTING.md`].


## License

Versions is licensed under the [Apache 2.0] license, a copy of which can be
found in the [`LICENSE`] file.

[`CONTRIBUTING.md`]: https://git.sr.ht/~samwhited/versions/tree/master/CONTRIBUTING.md
[Apache 2.0]: https://www.apache.org/licenses/LICENSE-2.0
[`LICENSE`]: https://git.sr.ht/~samwhited/versions/tree/master/LICENSE
