// Copyright 2019 Sam Whited
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! Contains icon names used by the main application.
//!
//! In a separate file for convenience, but reexported by res.

/// An icon used when files have been added.
pub const ICON_ADDED: &str = "list-add-symbolic";

/// An icon to use when files have been modified.
pub const ICON_MODIFIED: &str = "document-edit-symbolic";

/// An icon to use when files have been deleted.
pub const ICON_DELETED: &str = "list-remove-symbolic";

/// An icon to use when nearly identical files have been removed and added and rename detection is
/// on.
pub const ICON_RENAMED: &str = "object-flip-horizontal-symbolic";

/// An icon to show on the primary menu button.
pub const ICON_PRIMARY_MENU: &str = "open-menu-symbolic";

/// An icon to show on search buttons.
pub const ICON_SEARCH: &str = "system-search-symbolic";

/// An icon to show on open buttons.
pub const ICON_OPEN_DOCUMENT: &str = "document-open-symbolic";

/// An icon to show on "new" buttons.
pub const ICON_NEW_DOCUMENT: &str = "document-new-symbolic";

/// An icon to show on new buttons that add things to a list.
pub const ICON_LIST_ADD: &str = "list-add-symbolic";

/// An icon to show on things that save content, eg. saving a new revision.
pub const ICON_SAVE_DOCUMENT: &str = "document-save-symbolic";

/// An icon to show on views that can be refreshed.
pub const ICON_REFRESH: &str = "view-refresh-symbolic";
