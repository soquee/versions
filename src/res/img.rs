// Copyright 2019 Sam Whited
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! Contains images used by the main application.
//!
//! In a separate file for convenience, but reexported by res.

use crate::err::Result;

use gdk_pixbuf;
use gdk_pixbuf::PixbufLoaderExt;

/// A version of the logo for dark backgrounds.
const SVG_LOGO_DARK: &[u8] = include_bytes!("../../res/img/logo02_versions_dark.svg");

/// A version of the logo for light backgrounds.
const SVG_LOGO_LIGHT: &[u8] = include_bytes!("../../res/img/logo02_versions_light.svg");

/// Loads the light and dark version of the logos included in the binary into a pixbuf.
pub fn load_logos() -> Result<(Option<gdk_pixbuf::Pixbuf>, Option<gdk_pixbuf::Pixbuf>)> {
    let logobuf_light = {
        let logoloader = gdk_pixbuf::PixbufLoader::new();
        logoloader.write(SVG_LOGO_LIGHT)?;
        logoloader.close()?;

        logoloader.get_pixbuf()
    };
    let logobuf_dark = {
        let logoloader = gdk_pixbuf::PixbufLoader::new();
        logoloader.write(SVG_LOGO_DARK)?;
        logoloader.close()?;

        logoloader.get_pixbuf()
    };

    Ok((logobuf_light, logobuf_dark))
}
