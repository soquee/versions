// Copyright 2019 Sam Whited
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! Contains mostly static resources that may be used throughout the application.

mod cfg;
pub use cfg::*;

mod icons;
pub use icons::*;

mod img;
pub use img::*;

mod style;
pub use style::*;

/// The name of the application.
pub const APP_NAME: &str = "Versions";

/// The GTK application ID.
pub const APP_ID: &str = "net.soquee.versions";

/// The version of the app.
pub const VERSION: &str = env!("CARGO_PKG_VERSION");

/// The maximum commit length to allow in the commit summary text entry.
pub const MAX_COMMIT_LENGTH: i32 = 50;
