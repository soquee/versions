// Copyright 2019 Sam Whited
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use crate::err;
use crate::res;
use crate::ui;

use std::path;

use git2;
use git2::Status;

/// Creates a view for adding files and populates it with data from a projects index.
pub fn populate_add_files(project: &ui::Project, repo: &git2::Repository) {
    project.clear_add_files();
    let mut opts = git2::StatusOptions::new();
    let opts = opts
        .include_ignored(true)
        .include_untracked(true)
        .recurse_untracked_dirs(true);
    if let Ok(statuses) = repo.statuses(Some(opts)) {
        for entry in statuses.iter() {
            if let Some(p) = entry.path() {
                let status = entry.status();
                let icon_name = match status {
                    status
                        if status.intersects(
                            Status::WT_MODIFIED
                                | Status::INDEX_MODIFIED
                                | Status::WT_TYPECHANGE
                                | Status::INDEX_TYPECHANGE,
                        ) =>
                    {
                        res::ICON_MODIFIED
                    }
                    status if status.intersects(Status::WT_NEW | Status::INDEX_NEW) => {
                        res::ICON_ADDED
                    }
                    status if status.intersects(Status::WT_DELETED | Status::INDEX_DELETED) => {
                        res::ICON_DELETED
                    }
                    status if status.intersects(Status::WT_RENAMED | Status::INDEX_RENAMED) => {
                        res::ICON_RENAMED
                    }
                    _ => "",
                };
                project.append_add_files(None, icon_name, p, !status.contains(Status::IGNORED));
            }
        }
        project.autosize_add_files();
    }

    let author = match repo.signature() {
        Ok(sig) => format!("{}", sig),
        Err(_) => "".to_string(),
    };
    project.show_add_files(&author);
}

/// Populates a project widget with data from a real Git repo.
///
/// Meant to be set as part of a sidebars `selection-changed` callback.
pub fn populate_commit_info(project: &ui::Project, repo: &git2::Repository, hash: &str) {
    if let Ok(oid) = git2::Oid::from_str(&hash) {
        if let Ok(commit) = repo.find_commit(oid) {
            let commit_time = commit.time().seconds();
            let final_time = fmttime!(commit_time);
            let author = commit.author();
            let author_pretty = format!("{}", author);

            let details = ui::Revision::new(
                commit.message().unwrap_or_default(),
                &final_time,
                &author_pretty,
                false,
            );
            let diff = ui::Revision::new(
                commit.message().unwrap_or_default(),
                &final_time,
                &author_pretty,
                true,
            );

            if let Ok(tree) = commit.tree() {
                // Populate the diff view.
                if let Ok(parent) = commit.parent(0) {
                    if let Ok(parent_tree) = parent.tree() {
                        let mut diff_opts_base = git2::DiffOptions::new();
                        let diff_opts = diff_opts_base.force_binary(true);
                        if let Ok(diff_prev) = repo.diff_tree_to_tree(
                            Some(&parent_tree),
                            Some(&tree),
                            diff_opts.into(),
                        ) {
                            for delta in diff_prev.deltas() {
                                let new_file = delta
                                    .new_file()
                                    .path()
                                    .unwrap_or_else(|| path::Path::new(""))
                                    .to_str()
                                    .unwrap_or_default();
                                let old_file = delta
                                    .new_file()
                                    .path()
                                    .unwrap_or_else(|| path::Path::new(""))
                                    .to_str()
                                    .unwrap_or_default();
                                let (icon, line) = match delta.status() {
                                    git2::Delta::Added => (res::ICON_ADDED, new_file),
                                    git2::Delta::Deleted => (res::ICON_DELETED, old_file),
                                    git2::Delta::Modified => (res::ICON_MODIFIED, old_file),
                                    //git2::Delta::Renamed => "=>",
                                    //git2::Delta::Copied => "<=>",
                                    _ => ("", ""),
                                };
                                if !line.is_empty() {
                                    let _ = diff.append(None, icon, line);
                                }
                            }
                        }
                    }
                }

                // Populate the all files view if this commit has a tree that's
                // not empty.
                if !tree.is_empty() {
                    let mut vec = Vec::new();
                    let _ = tree.walk(git2::TreeWalkMode::PreOrder, |root, entry| {
                        let path = path::Path::new(root);
                        let path_len = path.components().count();
                        if path_len == 0 {
                            vec.clear();
                        }
                        let current = vec.pop();
                        let c =
                            details.append(current.as_ref(), "", entry.name().unwrap_or_default());
                        if path_len >= vec.len() {
                            if let Some(cur) = current {
                                vec.push(cur);
                            }
                        }
                        if entry.kind() == Some(git2::ObjectType::Tree) {
                            vec.push(c);
                        }
                        git2::TreeWalkResult::Ok
                    });
                }
            }

            project.set_children(diff.as_ref(), details.as_ref());
        }
    }
}

/// Formats data from a commit into a title and hash for the sidebar.
fn sidebar_values(commit: &git2::Commit) -> (String, String) {
    let commit_time = commit.time().seconds();
    let final_time = fmttime!(commit_time);
    let title = markup!(
        "<span weight='bold'>{}</span>\n{}",
        &final_time,
        commit.summary().unwrap_or_default()
    );
    let hash = format!("{}", commit.id());

    (title, hash)
}

/// Save a new revision (git commit).
pub fn save_revision(project: &ui::Project, repo: &git2::Repository) -> err::Result<()> {
    let commit_msg = project.get_msg();
    if commit_msg == "" {
        project.warn_no_msg();
        // This isn't an error, it's expected behavior, so just warn the user and short circuit.
        return Ok(());
    }

    let sig = repo.signature()?;
    let mut idx = repo.index()?;
    for f in project.selected_files() {
        let p = path::Path::new(&f);
        // If getting the status returns an error, it's not the end of the world. Just skip this
        // file.
        let status = match repo.status_file(p) {
            Ok(status) => status,
            Err(_) => continue,
        };
        match status {
            git2::Status::INDEX_DELETED | git2::Status::WT_DELETED => idx.remove_path(p)?,
            git2::Status::INDEX_NEW
            | git2::Status::WT_NEW
            | git2::Status::INDEX_TYPECHANGE
            | git2::Status::WT_TYPECHANGE
            | git2::Status::INDEX_MODIFIED
            | git2::Status::WT_MODIFIED => idx.add_path(p)?,
            _ => {}
        }
    }
    let oid = idx.write_tree()?;
    let tree = repo.find_tree(oid)?;

    let head = repo.head()?.peel_to_commit()?;
    let commit_oid = repo.commit(Some("HEAD"), &sig, &sig, &commit_msg, &tree, &[&head])?;
    let commit = repo.find_commit(commit_oid)?;
    repo.reset(commit.as_object(), git2::ResetType::Mixed, None)?;
    populate_add_files(project, repo);

    let (title, hash) = sidebar_values(&commit);
    project.sidebar_insert(0, &title, &hash);
    project.clear_msg();

    Ok(())
}

/// Insert revisions into the sidebar by walking the tree.
/// If oid is none, start from HEAD, if not, oid must be a committish on repo.
pub fn insert_revs(project: &ui::Project, repo: &git2::Repository) -> err::Result<()> {
    let mut revwalker = repo.revwalk()?;
    revwalker.set_sorting(git2::Sort::TOPOLOGICAL | git2::Sort::TIME);
    revwalker.push_head()?;
    for o in revwalker {
        let oid = o?;
        let commit = match repo.find_commit(oid) {
            Ok(commit) => commit,
            // This is not a commit or we got a bad OID. Skip it.
            Err(_) => continue,
        };
        let (title, hash) = sidebar_values(&commit);

        project.sidebar_insert(None, &title, &hash);
    }
    Ok(())
}
