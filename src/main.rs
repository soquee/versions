// Copyright 2019 Sam Whited
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! A version control system based on [Git] with a GUI written in [GTK+] using [Gtk-rs].
//!
//! Versions primarily targets users with lots of large files that change infrequently,
//! particularly the film industry.
//!
//! [Git]: https://git-scm.com/
//! [GTK+]: https://www.gtk.org/
//! [Gtk-rs]: https://gtk-rs.org/

#![deny(
    bad_style,
    missing_copy_implementations,
    const_err,
    dead_code,
    improper_ctypes,
    missing_debug_implementations,
    missing_docs,
    no_mangle_generic_items,
    non_shorthand_field_patterns,
    overflowing_literals,
    path_statements,
    patterns_in_fns_without_body,
    private_in_public,
    trivial_casts,
    trivial_numeric_casts,
    unconditional_recursion,
    unused,
    unused_allocation,
    unused_comparisons,
    unused_extern_crates,
    unused_import_braces,
    unused_parens,
    unused_qualifications,
    while_true
)]

#[macro_use]
mod macros;
mod app;
mod err;
mod git;
pub mod res;
mod ui;
mod widget;
mod window;

use std::env::args;
use std::process;

// This is the main entrypoint for the program. It should handle anything that needs to happen
// before GTK takes over, for instance, OS signals, dealing with command line args, and exiting the
// process on errors (nothing else should ever call process::exit).
fn main() {
    gtk::init().expect("Failed to start GTK. Please install GTK3.");
    match app::App::new() {
        Err(e) => {
            eprintln!("Error while registering GTK3 application: {:?}", e);
            process::exit(1);
        }
        Ok(app) => {
            app.run(&args().collect::<Vec<_>>());
        }
    }
}
