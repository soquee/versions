// Copyright 2019 Sam Whited
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use crate::err;
use crate::res;
use crate::ui;
use crate::window;

use glib;

use gdk;

use gio;
use gio::ActionMapExt;
use gio::ApplicationExt;
use gio::ApplicationExtManual;
use gio::FileExt;

use gtk;
use gtk::CssProviderExt;
use gtk::DialogExt;
use gtk::GtkApplicationExt;
use gtk::GtkWindowExtManual;
use gtk::SettingsExt;
use gtk::WidgetExt;

/// The app handles creating the UI and reacting to GIO signals.
#[derive(Debug, Clone)]
pub struct App {
    app: gtk::Application,
}

impl App {
    /// Creates the main application and reacts to the GIO activation signal to populate a window
    /// with a header bar and a view area where the various application panes can be rendered.
    pub fn new() -> err::Result<Self> {
        let app = gtk::Application::new(Some(res::APP_ID), gio::ApplicationFlags::HANDLES_OPEN)?;
        let me = Self { app };

        // About action
        me.new_action("about", {
            let (logobuf_light, logobuf_dark) = res::load_logos().unwrap_or((None, None));
            let about = ui::new_about_dialog(None::<&gtk::Window>, logobuf_light, logobuf_dark);
            move |_, _| Self::on_about(&about)
        });

        // Preferences action
        me.new_action("prefs", {
            let preferences = ui::Preferences::new();
            move |_, _| Self::on_prefs(&preferences)
        });

        if let Some(settings) = gtk::Settings::get_default() {
            settings.set_property_gtk_application_prefer_dark_theme(true);
        }

        if let Some(display) = gdk::Display::get_default() {
            let screen = display.get_default_screen();
            let style_provider = gtk::CssProvider::new();
            style_provider.load_from_data(res::STYLE_MAIN)?;
            gtk::StyleContext::add_provider_for_screen(
                &screen,
                &style_provider,
                gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
            );
        }

        me.app.connect_startup(|_| {});
        me.app.connect_activate(Self::on_activate);
        me.app.connect_open(Self::on_open);

        me.app.register(None::<&gio::Cancellable>)?;
        Ok(me)
    }

    /// A callback to invoke when the about action is triggered.
    fn on_about(about: &gtk::AboutDialog) {
        match about.get_window() {
            Some(_) => {
                about.present();
            }
            None => {
                about.run();
            }
        }
    }

    /// A callback to invoke when the prefs action is triggered.
    fn on_prefs(preferences: &ui::Preferences) {
        match preferences.get_window() {
            Some(_) => {
                preferences.present();
            }
            None => {
                preferences.run();
            }
        }
    }

    /// A callback to invoke when the app is activated.
    fn on_activate(app: &gtk::Application) {
        let window = window::Window::new(app);

        if window.open_last_project().is_ok() {
            app.add_window(window.as_ref());
        }
    }

    /// A callback to invoke when files should be opened.
    fn on_open(app: &gtk::Application, files: &[gio::File], _hint: &str) {
        for file in files {
            let window = window::Window::new(app);
            if let Some(path) = file.get_path() {
                if window.open_project_file(&path).is_ok() {
                    app.add_window(window.as_ref());
                }
            }
        }
    }

    /// Run the application.
    pub fn run(&self, argv: &[String]) -> i32 {
        self.app.run(argv)
    }

    /// Creates a simple action and connects the given handler to its activate signal.
    pub fn new_action<F: Fn(&gio::SimpleAction, &Option<glib::Variant>) + 'static>(
        &self,
        name: &str,
        f: F,
    ) {
        let action = gio::SimpleAction::new(name, None);
        action.connect_activate(f);
        self.add_action(&action);
    }
}

impl AsRef<gtk::Application> for App {
    #[inline]
    fn as_ref(&self) -> &gtk::Application {
        &self.app
    }
}

impl ActionMapExt for App {
    fn add_action<P: gtk::IsA<gio::Action>>(&self, action: &P) {
        self.app.add_action(action);
    }
    fn lookup_action(&self, action_name: &str) -> Option<gio::Action> {
        self.app.lookup_action(action_name)
    }
    fn remove_action(&self, action_name: &str) {
        self.app.remove_action(action_name);
    }
}
