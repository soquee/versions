// Copyright 2019 Sam Whited
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use crate::err;
use crate::git;
use crate::res;
use crate::ui;

use std::path;
use std::sync;

use gio;
use gio::ActionMapExt;

use git2;

use gtk;
use gtk::BoxExt;
use gtk::ContainerExt;
use gtk::FileChooserExt;
use gtk::GtkWindowExt;
use gtk::NativeDialogExt;
use gtk::TreeModelExt;
use gtk::TreeSelectionExt;
use gtk::WidgetExt;

/// A window that our application can open that contains the main project view.
#[derive(Debug, Clone)]
pub struct Window {
    window: gtk::ApplicationWindow,
    notify: ui::Notifier,
    container: gtk::Box,
    header_bar: ui::SplitHeaderBar,
}

impl Window {
    /// Create a new window and assign it to the given application.
    pub fn new(app: &gtk::Application) -> Self {
        let header_bar = ui::SplitHeaderBar::new();
        let hbox = header_bar.get_paned();

        let notify = ui::Notifier::new();
        let window = gtk::ApplicationWindow::new(app);
        window.set_default_size(1280, 720);
        window.set_position(gtk::WindowPosition::Center);
        window.set_title(res::APP_NAME);
        window.set_titlebar(hbox);

        let main_box = gtk::Box::new(gtk::Orientation::Vertical, 0);
        let container = gtk::Box::new(gtk::Orientation::Vertical, 0);

        main_box.add(notify.as_ref());
        main_box.add(&container);
        window.add(&main_box);

        window.show_all();

        let win = Self {
            window,
            notify,
            container,
            header_bar,
        };

        // Open button
        win.new_action("open", clone!(win => move |_, _| win.on_open()));

        // New Project button
        win.new_action("new_project", clone!(win => move |_, _| win.on_new()));

        win
    }

    /// A callback to invoke when the "open project" action is triggered.
    fn on_open(&self) {
        let p = gtk::FileChooserNative::new(
            "Open Project",
            &self.window,
            gtk::FileChooserAction::SelectFolder,
            "Open",
            "Cancel",
        );
        if p.run() == gtk::ResponseType::Accept.into() {
            match p.get_filename() {
                Some(file) => {
                    if self.open_project_file(&file).is_err() {
                        self.show_error("Error opening project.");
                    }
                }
                None => self.show_error("No project was selected."),
            }
        }
        p.destroy();
    }

    /// A callback to invoke when the "new project" action is triggered.
    fn on_new(&self) {
        let p = gtk::FileChooserNative::new(
            "New Project",
            &self.window,
            gtk::FileChooserAction::SelectFolder,
            "New",
            "Cancel",
        );
        if p.run() == gtk::ResponseType::Accept.into() {
            match p.get_filename() {
                Some(file) => {
                    if self.new_project(&file).is_err() {
                        self.show_error("Error creating project.");
                    }
                }
                None => self.show_error("No directory was selected."),
            }
        }
        p.destroy();
    }

    /// Show's an error message using an in app notification.
    pub fn show_error(&self, msg: &str) {
        self.notify.show_error(msg);
    }

    /// Sets the main view of the application window.
    pub fn set_view<P: gtk::IsA<gtk::Widget>>(&self, widget: &P) {
        for w in self.container.get_children() {
            self.container.remove(&w);
        }

        self.container.add(widget);
        self.container
            .set_child_packing(widget, true, true, 0, gtk::PackType::Start);
    }

    /// Gets the sidebar and main pane header bars.
    pub fn get_header_bar(&self) -> ui::SplitHeaderBar {
        self.header_bar.clone()
    }

    /// Creates a simple action and connects the given handler to its activate signal.
    pub fn new_action<F: Fn(&gio::SimpleAction, &Option<glib::Variant>) + 'static>(
        &self,
        name: &str,
        f: F,
    ) {
        let action = gio::SimpleAction::new(name, None);
        action.connect_activate(f);
        self.add_action(&action);
    }

    /// Opens an existing repo, creates a project view, and adds it to the container.
    pub fn open_project(&self, repo: git2::Repository) -> err::Result<()> {
        let header_bar = self.get_header_bar();
        let project = ui::Project::new(&header_bar);

        // Load all commits into sidebar.
        git::insert_revs(&project, &repo)?;

        let repocell = sync::Arc::new(sync::Mutex::new(repo));
        // When a commit is selected in the sidebar, load more info in the main pane.
        project.connect_selection_changed(
            clone!(repocell, project => move |selection| Self::on_selection_changed(&selection, &repocell, &project)),
        );

        self.new_action(
            "show_unsaved",
            clone!(project, repocell => move |_, _| Self::on_show_unsaved(&repocell, &project)),
        );

        let notify = self.notify.clone();
        self.new_action(
            "save_revision",
            clone!( project => move |_, _| Self::on_save_revision(&repocell, &project, &notify)),
        );

        self.set_view(project.as_ref());
        Ok(())
    }

    /// A callback to trigger when the "Save Revision" action is triggered.
    fn on_save_revision(
        repocell: &sync::Arc<sync::Mutex<git2::Repository>>,
        project: &ui::Project,
        notify: &ui::Notifier,
    ) {
        let repo = repocell.lock().unwrap();
        let err = git::save_revision(&project, &repo);
        if err.is_err() {
            notify.show_error(
                "Error saving revision. Are your username and email configured in preferences?",
            );
        }
    }

    /// A callback to invoke when the "Show Unsaved Changes" action is triggered.
    fn on_show_unsaved(repocell: &sync::Arc<sync::Mutex<git2::Repository>>, project: &ui::Project) {
        project.sidebar_unselect_all();
        let repo = repocell.lock().unwrap();
        git::populate_add_files(&project, &repo)
    }

    /// A callback to invoke when the project sidebar selection changes.
    fn on_selection_changed(
        selection: &gtk::TreeSelection,
        repocell: &sync::Arc<sync::Mutex<git2::Repository>>,
        project: &ui::Project,
    ) {
        match selection.get_selected() {
            Some((model, iter)) => {
                if let Some(hash) = model.get_value(&iter, 1).get::<String>() {
                    let repo = repocell.lock().unwrap();
                    git::populate_commit_info(&project, &repo, &hash);
                }
                project.show_stack();
            }
            None => project.show_placeholder(),
        }
    }

    /// Opens an existing repo from a file path.
    ///
    /// See [`open_project`] for more information.
    ///
    /// [`open_project`]: #method.open_project
    pub fn open_project_file(&self, filename: &path::Path) -> err::Result<()> {
        let repo = git2::Repository::open(&filename)?;
        self.open_project(repo)?;

        // If we can't save the location of the project we just opened for some reason, it's not
        // the end of the world. Continue and ignore the error.
        let _ = self.save_last_project(filename);
        Ok(())
    }

    /// Opens the last opened project (if found) by reading the XDG git config file.
    ///
    /// If the project does not exist or is not set, no error is returned because it is perfectly
    /// possible that a user just moved the project somewhere else or it can't be found for any
    /// number of other reasons.
    pub fn open_last_project(&self) -> err::Result<()> {
        let mut config = git2::Config::open_default()?;
        let snapshot = config.snapshot()?;
        let path = path::Path::new(snapshot.get_str(res::CFG_LAST_OPEN).unwrap_or_default());
        if path.to_str().unwrap_or_default() == "" {
            return Ok(());
        }
        if self.open_project_file(path).is_err() {
            if let Ok(mut global) = config.open_global() {
                global.remove(res::CFG_LAST_OPEN)?;
            }
        }
        Ok(())
    }

    /// Save the last opened project to Git config.
    pub fn save_last_project(&self, filename: &path::Path) -> err::Result<()> {
        if let Some(path) = filename.to_str() {
            let mut config = git2::Config::open_default()?;
            let mut global = config.open_global()?;
            global.set_str(res::CFG_LAST_OPEN, path)?;
        }
        Ok(())
    }

    /// Creates a new repo in the given directory and then opens it.
    pub fn new_project(&self, filename: &path::Path) -> err::Result<()> {
        let mut base_opts = git2::RepositoryInitOptions::new();
        let opts = base_opts.no_reinit(true).mkdir(false).mkpath(false);
        let repo = git2::Repository::init_opts(&filename, &opts)?;

        let sig = repo.signature()?;
        let empty_tree_oid = {
            let mut idx = repo.index()?;
            idx.write_tree()?
        };
        {
            let empty_tree = repo.find_tree(empty_tree_oid)?;
            repo.commit(
                Some("HEAD"),
                &sig,
                &sig,
                "Created new project",
                &empty_tree,
                &[],
            )?;
        }
        self.open_project(repo)?;

        // If we can't save the location of the project we just created for some reason, it's not
        // the end of the world. Continue and ignore the error.
        let _ = self.save_last_project(filename);
        Ok(())
    }
}

impl AsRef<gtk::ApplicationWindow> for Window {
    #[inline]
    fn as_ref(&self) -> &gtk::ApplicationWindow {
        &self.window
    }
}

impl ActionMapExt for Window {
    fn add_action<P: gtk::IsA<gio::Action>>(&self, action: &P) {
        self.window.add_action(action);
    }
    fn lookup_action(&self, action_name: &str) -> Option<gio::Action> {
        self.window.lookup_action(action_name)
    }
    fn remove_action(&self, action_name: &str) {
        self.window.remove_action(action_name);
    }
}
