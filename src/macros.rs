// Copyright 2019 Sam Whited
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/// Makes it easier to clone GTK objects before moving them into closures.
/// Borrowed from the gtk-rs examples.
///
/// ```rust,ignore
/// let about = gtk::AboutDialog::new();
/// if let Some(settings) = about.get_settings() {
///     settings.connect_property_gtk_application_prefer_dark_theme_notify(
///         clone!( about, logo_dark, logo_light => move |settings| {
///             if settings.get_property_gtk_application_prefer_dark_theme() {
///                 about.set_logo(&logo_dark);
///             } else {
///                 about.set_logo(&logo_light);
///             }
///         }),
///     ),
/// }
/// ```
macro_rules! clone {
    (@param _) => ( _ );
    (@param $x:ident) => ( $x );
    ($($n:ident),+ => move || $body:expr) => (
        {
            $( let $n = $n.clone(); )+
            move || $body
        }
    );
    ($($n:ident),+ => move |$($p:tt),+| $body:expr) => (
        {
            $( let $n = $n.clone(); )+
            move |$(clone!(@param $p),)+| $body
        }
    );
}

/// Format a commit time in a standard way.
macro_rules! fmttime {
    ($t:expr) => {
        format!(
            "{}",
            chrono::naive::NaiveDateTime::from_timestamp($t, 0).format("%Y–%m–%d")
        )
    };
}

/// Like format! but only takes &str arguments which are escaped for use in
/// [The Pango Markup Language].
///
/// [The Pango Markup Language]: https://developer.gnome.org/pygtk/stable/pango-markup-language.html
macro_rules! markup {
    ($fmt:expr, $($arg:expr),*$(,)?) => ({
        use glib;
        std::fmt::format(format_args!($fmt, $(
            glib::markup_escape_text($arg)
        ),*
        ))
    })
}
