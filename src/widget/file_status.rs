// Copyright 2019 Sam Whited
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use gtk;
use gtk::CellLayoutExt;
use gtk::TreeModelExt;
use gtk::TreeSelectionExt;
use gtk::TreeStoreExt;
use gtk::TreeStoreExtManual;
use gtk::TreeViewColumnExt;
use gtk::TreeViewExt;
use gtk::WidgetExt;

/// A scrolled tree view containing files with basic status information like `git status`.
#[derive(Debug, Clone)]
pub struct FileStatus {
    tree_view: gtk::TreeView,
    model: gtk::TreeStore,
}

impl FileStatus {
    /// Create a new file status view.
    pub fn new(mode: gtk::SelectionMode, show_icon: bool) -> Self {
        let (tree_view, model) = {
            // Changes col, file name, file path, sensitive (not greyed out)
            let model =
                gtk::TreeStore::new(&[gtk::Type::String, gtk::Type::String, gtk::Type::Bool]);

            let renderer = gtk::CellRendererText::new();
            let files_col = gtk::TreeViewColumn::new();
            files_col.set_sort_column_id(0);
            files_col.set_title("Files");
            files_col.pack_start(&renderer, true);
            files_col.add_attribute(&renderer, "text", 1);
            files_col.add_attribute(&renderer, "sensitive", 2);

            let tree_view = {
                let tree_view = gtk::TreeView::new_with_model(&model);
                tree_view.set_headers_visible(true);
                tree_view.set_hexpand(true);
                tree_view.set_vexpand(true);
                tree_view.set_enable_search(false);

                if show_icon {
                    let renderer = gtk::CellRendererPixbuf::new();
                    let changes_col = gtk::TreeViewColumn::new();
                    changes_col.set_sort_column_id(0);
                    changes_col.set_title("Change");
                    changes_col.pack_start(&renderer, true);
                    changes_col.add_attribute(&renderer, "icon-name", 0);
                    tree_view.append_column(&changes_col);
                }
                tree_view.append_column(&files_col);

                tree_view
            };

            let selection = tree_view.get_selection();
            selection.set_mode(mode);
            selection.set_select_function(Some(Box::new(Self::on_select)));

            (tree_view, model)
        };

        Self { tree_view, model }
    }

    /// Callback that gets run when the user tries to change the selection.
    /// If the item is greyed out (it's .gitignored), don't allow it to be selected.
    fn on_select(
        _: &gtk::TreeSelection,
        model: &gtk::TreeModel,
        path: &gtk::TreePath,
        selected: bool,
    ) -> bool {
        if let Some(iter) = model.get_iter(&path) {
            let sensitive = model.get_value(&iter, 2).get::<bool>().unwrap_or_default();
            if !sensitive {
                return false;
            }
            if model.iter_has_child(&iter) {
                return false;
            }
            return !selected;
        }
        false
    }

    /// Resizes all columns to their optimal width.
    /// Only works after the treeview has been realized.
    pub fn columns_autosize(&self) {
        self.tree_view.columns_autosize()
    }

    /// Returns the underlying model used for the tree view.
    pub fn get_model(&self) -> &gtk::TreeStore {
        &self.model
    }

    /// Returns the selection for the tree view.
    pub fn get_selection(&self) -> gtk::TreeSelection {
        self.tree_view.get_selection()
    }

    /// Append a column to the tree view.
    pub fn append<'a, P>(&self, parent: P, icon: &str, name: &str, sensitive: bool) -> gtk::TreeIter
    where
        P: Into<Option<&'a gtk::TreeIter>>,
    {
        let icon_val = gtk::ToValue::to_value(icon);
        let name_val = gtk::ToValue::to_value(name);
        let sensitive_val = gtk::ToValue::to_value(&sensitive);

        let tree_iter = self.model.append(parent);
        self.model.set_value(&tree_iter, 0, &icon_val);
        self.model.set_value(&tree_iter, 1, &name_val);
        self.model.set_value(&tree_iter, 2, &sensitive_val);

        tree_iter
    }
}

impl AsRef<gtk::TreeView> for FileStatus {
    #[inline]
    fn as_ref(&self) -> &gtk::TreeView {
        &self.tree_view
    }
}
