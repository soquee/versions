// Copyright 2019 Sam Whited
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use crate::res;
use crate::ui::legend;
use crate::widget;

use gtk;
use gtk::ContainerExt;
use gtk::EditableSignals;
use gtk::EntryExt;
use gtk::ScrolledWindowExt;
use gtk::StyleContextExt;
use gtk::TextBufferExt;
use gtk::TextViewExt;
use gtk::TreeModelExt;
use gtk::TreeSelectionExt;
use gtk::TreeStoreExt;
use gtk::WidgetExt;

/// A widget for displaying a list of pending changes that can be added to a new revision.
#[derive(Debug, Clone)]
pub struct Add {
    view: gtk::Box,
    files_tree: widget::FileStatus,
    author: gtk::Box,
    msg: gtk::Entry,
    long_msg: gtk::TextView,
}

impl Add {
    /// Create a view for displaying unsaved files and adding them to a revision.
    pub fn new() -> Self {
        let msg = {
            let msg = gtk::Entry::new();
            msg.set_input_purpose(gtk::InputPurpose::FreeForm);
            msg.set_max_length(res::MAX_COMMIT_LENGTH);
            msg.set_placeholder_text("Short description");
            msg.set_input_hints(
                gtk::InputHints::SPELLCHECK
                    | gtk::InputHints::WORD_COMPLETION
                    | gtk::InputHints::EMOJI,
            );
            msg.connect_changed(|entry| {
                let len = match entry.get_text() {
                    Some(txt) => txt.len(),
                    None => 0,
                };
                entry.set_progress_fraction(len as f64 / f64::from(res::MAX_COMMIT_LENGTH));
                let ctx = entry.get_style_context();
                if len == 0 {
                    ctx.add_class("entry-invalid");
                } else {
                    ctx.remove_class("entry-invalid");
                }
            });
            msg
        };

        let (scrolled_msg, long_msg) = {
            let msg = gtk::TextView::new();
            msg.set_input_purpose(gtk::InputPurpose::FreeForm);
            msg.set_wrap_mode(gtk::WrapMode::WordChar);
            msg.set_input_hints(
                gtk::InputHints::SPELLCHECK
                    | gtk::InputHints::WORD_COMPLETION
                    | gtk::InputHints::EMOJI,
            );
            msg.set_accepts_tab(false);

            let scrolled =
                gtk::ScrolledWindow::new(None::<&gtk::Adjustment>, None::<&gtk::Adjustment>);
            scrolled.set_policy(gtk::PolicyType::Never, gtk::PolicyType::Automatic);
            scrolled.set_hexpand(true);
            scrolled.add(&msg);
            (scrolled, msg)
        };

        let author = gtk::Box::new(gtk::Orientation::Horizontal, 16);

        let files_tree = widget::FileStatus::new(gtk::SelectionMode::Multiple, true);

        let scrolled = {
            let scrolled =
                gtk::ScrolledWindow::new(None::<&gtk::Adjustment>, None::<&gtk::Adjustment>);
            scrolled.set_policy(gtk::PolicyType::Automatic, gtk::PolicyType::Automatic);
            scrolled.add(files_tree.as_ref());
            scrolled
        };

        let legend_view = legend::new();

        let view = gtk::Box::new(gtk::Orientation::Vertical, 16);
        let ctx = view.get_style_context();
        ctx.add_class("content");
        view.add(&msg);
        view.add(&scrolled_msg);
        view.add(&author);
        view.add(&scrolled);
        view.add(&legend_view);
        view.show_all();

        Self {
            view,
            files_tree,
            author,
            msg,
            long_msg,
        }
    }

    /// Append a file to the tree view.
    pub fn append<'a, P: Into<Option<&'a gtk::TreeIter>>>(
        &self,
        parent: P,
        icon: &str,
        name: &str,
        sensitive: bool,
    ) -> gtk::TreeIter {
        self.files_tree.append(parent, icon, name, sensitive)
    }

    /// Clear all files from the tree view.
    pub fn clear(&self) {
        self.files_tree.get_model().clear();
    }

    /// Returns an iterator over selected file names in the tree view.
    pub fn selected_files(&self) -> Vec<String> {
        let selection = self.files_tree.get_selection();
        let (rows, _) = selection.get_selected_rows();
        rows.iter()
            .map(|row| {
                let model = self.files_tree.get_model();
                let iter = model.get_iter(row).unwrap();
                model.get_value(&iter, 1).get::<String>().unwrap()
            })
            .collect()
    }

    /// Autosize the columns on the treeview that shows new files.
    pub fn columns_autosize(&self) {
        self.files_tree.columns_autosize();
    }

    /// Set the name of the author.
    pub fn set_author(&self, author: &str) {
        for child in self.author.get_children() {
            self.author.remove(&child);
        }
        let label = {
            let label = gtk::Label::new(author);
            label.set_halign(gtk::Align::End);
            label.set_hexpand(true);
            let ctx = label.get_style_context();
            ctx.add_class("commit-authors");
            label.show();
            label
        };
        self.author.add(&label);
    }

    /// Clear the commit message entry fields.
    pub fn clear_msg(&self) {
        self.msg.set_text("");
        let buf = gtk::TextBuffer::new(None::<&gtk::TextTagTable>);
        self.long_msg.set_buffer(&buf);
    }

    /// Get the commit message entered by the user into this dialog by appending the message netry,
    /// an empty line, and then the longer text view.
    pub fn get_msg(&self) -> String {
        let long_msg = match self.long_msg.get_buffer() {
            Some(buf) => {
                let end = buf.get_end_iter();
                let start = buf.get_start_iter();
                buf.get_text(&start, &end, false)
                    .unwrap_or("".into())
                    .to_string()
            }
            None => "".to_string(),
        };
        let short_msg = self.msg.get_text().unwrap_or("".into()).to_string();
        // Don't return a message unless the summary has been entered.
        if short_msg == "" {
            return short_msg;
        }
        if long_msg != "" {
            return format!("{}\n\n{}", short_msg, long_msg);
        }
        short_msg
    }

    /// Highlights and selects the summary text entry to warn the user that it is required.
    pub fn warn_no_msg(&self) {
        self.msg.grab_focus();
    }
}

impl AsRef<gtk::Box> for Add {
    #[inline]
    fn as_ref(&self) -> &gtk::Box {
        &self.view
    }
}
