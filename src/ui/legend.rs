// Copyright 2019 Sam Whited
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use super::res;

use gtk;
use gtk::ContainerExt;

/// Create a new legend widget with variouos icons.
pub fn new() -> gtk::Box {
    let edit_img = gtk::Image::new_from_icon_name(res::ICON_MODIFIED, gtk::IconSize::Menu.into());
    let edit_label = gtk::Label::new(" file edited | ");
    let add_img = gtk::Image::new_from_icon_name(res::ICON_ADDED, gtk::IconSize::Menu.into());
    let add_label = gtk::Label::new(" file added | ");
    let rm_img = gtk::Image::new_from_icon_name(res::ICON_DELETED, gtk::IconSize::Menu.into());
    let rm_label = gtk::Label::new(" file removed");

    let legend = gtk::Box::new(gtk::Orientation::Horizontal, 0);
    legend.add(&edit_img);
    legend.add(&edit_label);
    legend.add(&add_img);
    legend.add(&add_label);
    legend.add(&rm_img);
    legend.add(&rm_label);

    legend
}
