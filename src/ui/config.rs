// Copyright 2019 Sam Whited
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use gdk;
use git2;

use gtk;
use gtk::ContainerExt;
use gtk::DialogExt;
use gtk::EntryExt;
use gtk::GtkWindowExtManual;
use gtk::StyleContextExt;
use gtk::WidgetExt;

/// The main preferences dialog.
#[derive(Debug, Clone)]
pub struct Preferences {
    notifier: super::Notifier,
    dialog: gtk::Dialog,
    user_entry: gtk::Entry,
    email_entry: gtk::Entry,
}

impl Preferences {
    /// Creates a new preferences view that can be reused.
    pub fn new() -> Self {
        let dialog = gtk::Dialog::new_with_buttons(
            "Preferences",
            None::<&gtk::Window>,
            gtk::DialogFlags::USE_HEADER_BAR,
            &[("Save", gtk::ResponseType::Apply.into())],
        );
        let content = dialog.get_content_area();

        if let Some(save) = dialog.get_widget_for_response(gtk::ResponseType::Apply.into()) {
            let ctx = save.get_style_context();
            ctx.add_class("suggested-action");
        }

        let user_label = gtk::Label::new("Username");
        let user_entry = gtk::Entry::new();
        user_entry.set_input_purpose(gtk::InputPurpose::Name);
        let user_box = gtk::Box::new(gtk::Orientation::Vertical, 8);
        user_box.add(&user_label);
        user_box.add(&user_entry);

        let email_label = gtk::Label::new("Email");
        let email_entry = gtk::Entry::new();
        email_entry.set_input_purpose(gtk::InputPurpose::Email);
        let email_box = gtk::Box::new(gtk::Orientation::Vertical, 8);
        email_box.add(&email_label);
        email_box.add(&email_entry);

        let mainbox = gtk::Box::new(gtk::Orientation::Vertical, 16);
        mainbox.add(&user_box);
        mainbox.add(&email_box);
        mainbox.set_margin_top(8);
        mainbox.set_margin_bottom(16);
        mainbox.set_margin_start(8);
        mainbox.set_margin_end(8);

        let notifier = super::Notifier::new();

        content.add(notifier.as_ref());
        content.add(&mainbox);
        mainbox.show_all();
        content.show_all();

        let me = Self {
            notifier,
            dialog,
            user_entry,
            email_entry,
        };

        // We expect to reuse this dialog across multiple windows, so don't destroy it it is closed.
        me.dialog
            .connect_delete_event(|d, _| gtk::Inhibit(d.hide_on_delete()));
        me.dialog.connect_response(clone!( me => move |d, resp| {
            match resp.into() {
                gtk::ResponseType::Apply => {
                    if me.save_config().is_err() {
                        me.notifier.show_error("Error saving config.");
                    } else {
                        d.hide();
                    }
                },
                _ => d.hide(),
            };
        }));

        me
    }

    /// Proxy through to the dialogs `run` method.
    pub fn run(&self) -> gtk::ResponseType {
        if self.load_config().is_err() {
            self.notifier.show_error("Error loading config.");
        }
        // Add notifier to preferences pane and show error there?
        self.dialog.run().into()
    }

    /// Proxy through to the dialogs `present` method.
    pub fn present(&self) {
        self.dialog.present()
    }

    /// Populates the dialog from the config file on disk.
    pub fn load_config(&self) -> super::Result<()> {
        let mut c = git2::Config::open_default()?;
        let snapshot = c.snapshot()?;
        self.user_entry
            .set_text(snapshot.get_str("user.name").unwrap_or_default());
        self.email_entry
            .set_text(snapshot.get_str("user.email").unwrap_or_default());
        Ok(())
    }

    pub fn save_config(&self) -> super::Result<()> {
        let mut c = git2::Config::open_default()?;
        let mut global = c.open_global()?;
        if let Some(username) = self.user_entry.get_text() {
            global.set_str("user.name", username.as_str())?;
        } else {
            global.set_str("user.name", "")?;
        }
        if let Some(email) = self.email_entry.get_text() {
            global.set_str("user.email", email.as_str())?;
        } else {
            global.set_str("user.email", "")?;
        }
        Ok(())
    }

    /// Proxy through to the dialogs `get_window` method.
    pub fn get_window(&self) -> Option<gdk::Window> {
        self.dialog.get_window()
    }
}
