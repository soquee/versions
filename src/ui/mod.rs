// Copyright 2019 Sam Whited
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

mod about;
mod add;
mod app_menu;
mod config;
mod header_bar;
mod legend;
mod notifier;
mod placeholder;
mod project;
mod revision;
mod sidebar;

pub use self::about::new_about_dialog;
pub use self::add::Add;
pub use self::app_menu::app_menu;
pub use self::config::Preferences;
pub use self::header_bar::SplitHeaderBar;
pub use self::notifier::Notifier;
pub use self::placeholder::new_placeholder;
pub use self::project::Project;
pub use self::revision::Revision;
pub use self::sidebar::Sidebar;
pub use super::err::Result;
pub use super::res;
