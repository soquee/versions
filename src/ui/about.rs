// Copyright 2019 Sam Whited
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use crate::res;
use gdk;
use gdk_pixbuf;

use gtk;
use gtk::AboutDialogExt;
use gtk::DialogExt;
use gtk::GtkWindowExt;
use gtk::HeaderBarExt;
use gtk::SettingsExt;
use gtk::WidgetExt;

pub fn new_about_dialog<'a, P: gtk::IsA<gtk::Window> + 'a, Q: Into<Option<&'a P>>>(
    parent: Q,
    logobuf_light: Option<gdk_pixbuf::Pixbuf>,
    logobuf_dark: Option<gdk_pixbuf::Pixbuf>,
) -> gtk::AboutDialog {
    let p = gtk::AboutDialog::new();
    p.set_authors(&["Soquee"]);
    p.set_copyright("Copyright © 2019 Soquee.\nAll rights reserved.");
    p.set_destroy_with_parent(true);
    //p.set_license_type(gtk::License::Custom);
    p.set_program_name(res::APP_NAME);
    p.set_skip_pager_hint(true);
    p.set_skip_taskbar_hint(true);
    p.set_title("About");
    p.set_transient_for(parent);
    p.set_type_hint(gdk::WindowTypeHint::Splashscreen);
    p.set_version(res::VERSION);
    p.set_website("https://soquee.net");
    p.set_website_label("soquee.net");
    p.add_credit_section(
        "Open Source",
        &[
            "Chrono https://crates.io/crates/chrono (MIT/Apache-2.0)",
            "Git2-rs https://github.com/alexcrichton/git2-rs (MIT/Apache-2.0)",
            "Git https://git-scm.com/ (GPLv2)",
            "Gtk-rs http://gtk-rs.org/ (MIT)",
            "Rust https://www.rust-lang.org/ (MIT/Apache-2.0)",
        ],
    );

    // We expect to reuse this dialog across multiple windows, so don't destroy it when the window
    // is closed.
    p.connect_delete_event(|p, _| gtk::Inhibit(p.hide_on_delete()));
    p.connect_response(|p, _| p.hide());

    // If the window manager doesn't use a header bar, slap one on there.
    if p.get_header_bar().is_none() {
        let hbar = gtk::HeaderBar::new();
        hbar.set_title(p.get_title().unwrap_or_else(|| "About".into()).as_str());
        hbar.show_all();
        p.set_titlebar(&hbar);
    }

    if let Some(logo_dark) = logobuf_dark {
        if let Some(logo_light) = logobuf_light {
            match p.get_settings() {
                Some(settings) => Some(
                    settings.connect_property_gtk_application_prefer_dark_theme_notify(
                        clone!( p, logo_dark, logo_light => move |settings| {
                            // TODO: I don't think this will always actually tell us if we're using
                            // the dark theme. We may have to try and do some blending or detect
                            // the background color.
                            if settings.get_property_gtk_application_prefer_dark_theme() {
                                p.set_logo(&logo_dark);
                            } else {
                                p.set_logo(&logo_light);
                            }
                        }),
                    ),
                ),
                None => None,
            };
        }
    }

    p
}
