// Copyright 2019 Sam Whited
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use gtk;
use gtk::WidgetExt;

/// An icon that will be displayed when on project is open or no commit selected.
pub fn new_placeholder() -> gtk::Image {
    let img =
        gtk::Image::new_from_icon_name("folder-templates-symbolic", gtk::IconSize::Dialog.into());
    img.set_vexpand(true);
    img.set_hexpand(true);
    img
}
