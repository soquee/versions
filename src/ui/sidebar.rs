// Copyright 2019 Sam Whited
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use crate::ui;

use glib;

use gtk;
use gtk::CellLayoutExt;
use gtk::ContainerExt;
use gtk::EntryExt;
use gtk::ObjectExt;
use gtk::ScrolledWindowExt;
use gtk::SearchEntryExt;
use gtk::TreeModelExt;
use gtk::TreeModelFilterExt;
use gtk::TreeSelectionExt;
use gtk::TreeViewColumnExt;
use gtk::TreeViewExt;
use gtk::WidgetExt;

/// A sidebar with a treeview and a search bar.
#[derive(Debug, Clone)]
pub struct Sidebar {
    view: gtk::Box,
    selection: gtk::TreeSelection,
}

impl Sidebar {
    /// Creates an new sidebar and wires up its search view.
    pub fn new<'a, P>(hbar: &ui::SplitHeaderBar, model: &P) -> Self
    where
        P: gtk::IsA<gtk::TreeModel> + 'a,
    {
        let tree = gtk::TreeView::new();
        tree.set_headers_visible(false);
        tree.set_hexpand(false);
        tree.set_vexpand(true);
        tree.set_enable_search(false);

        let renderer = gtk::CellRendererText::new();
        let col = gtk::TreeViewColumn::new();
        col.set_title("Revisions");
        col.pack_start(&renderer, true);
        col.add_attribute(&renderer, "markup", 0);
        tree.append_column(&col);

        let selection = tree.get_selection();

        let scrolled = gtk::ScrolledWindow::new(None::<&gtk::Adjustment>, None::<&gtk::Adjustment>);
        scrolled.set_policy(gtk::PolicyType::Automatic, gtk::PolicyType::Automatic);
        scrolled.add(&tree);

        let search_entry = gtk::SearchEntry::new();
        let search_bar = gtk::SearchBar::new();
        search_bar.add(&search_entry);

        let filter = gtk::TreeModelFilter::new(model, None);
        filter.set_visible_func(
            clone!( search_entry => move |model, iter| match search_entry.get_text() {
                Some(s) => {
                    if let Some(txt) = model.get_value(&iter, 0).get::<String>() {
                        txt.contains(s.as_str())
                    } else {
                        true
                    }
                }
                None => true,
            }),
        );
        tree.set_model(&filter);

        search_entry.connect_search_changed(move |_| {
            filter.refilter();
        });
        let search_button = hbar.get_search_button();
        search_bar
            .bind_property("search-mode-enabled", &search_button, "active")
            .flags(glib::BindingFlags::BIDIRECTIONAL)
            .build();

        let view = gtk::Box::new(gtk::Orientation::Vertical, 0);
        view.add(&search_bar);
        view.add(&scrolled);

        let shbar = gtk::HeaderBar::new();
        shbar.set_no_show_all(true);

        Sidebar { view, selection }
    }

    /// Register a handler to be called when an item is selected in the sidebar.
    ///
    /// One or more signals may be emitted when multiple items are selected, or a signal may be
    /// omitted for a row that is already selected so always check what the selection actually is.
    pub fn connect_selection_changed<F>(&self, f: F) -> glib::SignalHandlerId
    where
        F: Fn(&gtk::TreeSelection) + 'static,
    {
        self.selection.connect_changed(f)
    }

    /// Unselects all the nodes.
    pub fn unselect_all(&self) {
        self.selection.unselect_all();
    }
}

impl AsRef<gtk::Box> for Sidebar {
    #[inline]
    fn as_ref(&self) -> &gtk::Box {
        &self.view
    }
}
