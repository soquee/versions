// Copyright 2019 Sam Whited
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use crate::res;

use gio;
use gtk;
use gtk::ActionableExt;
use gtk::ButtonExt;
use gtk::ContainerExt;
use gtk::HeaderBarExt;
use gtk::MenuButtonExt;
use gtk::PanedExt;
use gtk::StyleContextExt;
use gtk::WidgetExt;

/// The header bar.
#[derive(Debug, Clone)]
pub struct SplitHeaderBar {
    hbox: gtk::Paned,
    shbar: gtk::HeaderBar,
    hbar: gtk::HeaderBar,
    search: gtk::ToggleButton,
    unsaved_changes_buttons: gtk::Box,
}

impl SplitHeaderBar {
    /// Populate and return a new split header bar.
    pub fn new() -> Self {
        // Create the side header bar and search button.
        let (shbar, search) = {
            let shbar = gtk::HeaderBar::new();
            shbar.set_no_show_all(true);

            let commit = {
                let commit = gtk::Button::new_from_icon_name(
                    res::ICON_LIST_ADD,
                    gtk::IconSize::Button.into(),
                );
                let ctx = commit.get_style_context();
                ctx.add_class("suggested-action");
                commit.set_action_name("win.show_unsaved");
                commit.set_tooltip_text("New Revision");
                commit.show();

                commit
            };
            shbar.pack_start(&commit);

            let search = {
                let search = gtk::ToggleButton::new();
                let search_icon =
                    gtk::Image::new_from_icon_name(res::ICON_SEARCH, gtk::IconSize::Button.into());
                search.set_image(&search_icon);
                search.set_tooltip_text("Search Revisions");
                search.show();
                shbar.pack_end(&search);

                search
            };

            (shbar, search)
        };

        let (hbar, unsaved_changes_buttons) = {
            let hbar = gtk::HeaderBar::new();
            hbar.set_title(res::APP_NAME);
            hbar.set_show_close_button(true);
            hbar.set_hexpand(true);

            let context_button = {
                let context_button = gtk::MenuButton::new();
                let menu = super::app_menu();

                context_button.set_tooltip_text("App Menu");
                let menu_icon = gio::ThemedIcon::new(res::ICON_PRIMARY_MENU);
                let menu_image =
                    gtk::Image::new_from_gicon(&menu_icon, gtk::IconSize::Button.into());
                context_button.set_image(&menu_image);
                context_button.set_menu_model(&menu);
                context_button.show();

                context_button
            };
            hbar.pack_end(&context_button);

            let open_button = gtk::Button::new_from_icon_name(
                res::ICON_OPEN_DOCUMENT,
                gtk::IconSize::Button.into(),
            );
            open_button.set_action_name("win.open");
            open_button.set_tooltip_text("Open Project");
            hbar.add(&open_button);

            let new_button = gtk::Button::new_from_icon_name(
                res::ICON_NEW_DOCUMENT,
                gtk::IconSize::Button.into(),
            );
            new_button.set_action_name("win.new_project");
            new_button.set_tooltip_text("New Project");
            hbar.add(&new_button);

            let unsaved_changes_buttons = {
                let save_revision = gtk::Button::new_from_icon_name(
                    res::ICON_SAVE_DOCUMENT,
                    gtk::IconSize::Button.into(),
                );
                let ctx = save_revision.get_style_context();
                ctx.add_class("suggested-action");
                save_revision.set_action_name("win.save_revision");
                save_revision.set_tooltip_text("Save Revision");
                save_revision.show();

                let refresh = gtk::Button::new_from_icon_name(
                    res::ICON_REFRESH,
                    gtk::IconSize::Button.into(),
                );
                refresh.set_action_name("win.show_unsaved");
                refresh.set_tooltip_text("Refresh");
                refresh.show();

                let unsaved_changes_buttons = gtk::Box::new(gtk::Orientation::Horizontal, 16);
                unsaved_changes_buttons.add(&refresh);
                unsaved_changes_buttons.add(&save_revision);
                unsaved_changes_buttons.set_no_show_all(true);

                unsaved_changes_buttons
            };
            hbar.pack_end(&unsaved_changes_buttons);

            (hbar, unsaved_changes_buttons)
        };

        let hbox = {
            let hbox = gtk::Paned::new(gtk::Orientation::Horizontal);
            hbox.set_can_focus(false);
            hbox.pack1(&shbar, false, false);
            hbox.pack2(&hbar, true, true);

            hbox
        };

        Self {
            hbox,
            shbar,
            hbar,
            search,
            unsaved_changes_buttons,
        }
    }

    /// Gets the pane containing both header bars.
    pub fn get_paned(&self) -> &gtk::Paned {
        &self.hbox
    }

    /// Gets the main header bar.
    pub fn get_main_bar(&self) -> &gtk::HeaderBar {
        &self.hbar
    }

    /// Gets the sidebar.
    pub fn get_sidebar(&self) -> &gtk::HeaderBar {
        &self.shbar
    }

    /// Returns the search button.
    pub fn get_search_button(&self) -> gtk::ToggleButton {
        self.search.clone()
    }

    /// Returns the commit button.
    pub fn get_unsaved_changes_buttons(&self) -> gtk::Box {
        self.unsaved_changes_buttons.clone()
    }
}

impl AsRef<gtk::Paned> for SplitHeaderBar {
    #[inline]
    fn as_ref(&self) -> &gtk::Paned {
        &self.hbox
    }
}
