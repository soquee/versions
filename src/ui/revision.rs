// Copyright 2019 Sam Whited
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use crate::ui::legend;
use crate::widget;

use gtk::ContainerExt;
use gtk::ScrolledWindowExt;
use gtk::StyleContextExt;
use gtk::WidgetExt;

/// A widget for displaying a revision and its files (or changes).
pub struct Revision {
    view: gtk::Box,
    files_tree: widget::FileStatus,
}

impl Revision {
    /// Create an new view for displaying all files.
    pub fn new(msg: &str, datetime: &str, author: &str, show_changes: bool) -> Self {
        let files_tree = widget::FileStatus::new(gtk::SelectionMode::None, show_changes);

        let desc = {
            let desc = gtk::Label::new(msg);
            let ctx = desc.get_style_context();
            ctx.add_class("commit-msg");
            desc
        };

        let time_author = {
            let time_author = gtk::Box::new(gtk::Orientation::Horizontal, 0);
            let ctx = time_author.get_style_context();
            ctx.add_class("commit-time-author");
            time_author
        };

        let time = {
            let time = gtk::Label::new(datetime);
            time.set_halign(gtk::Align::Start);
            time.set_hexpand(true);
            let ctx = time.get_style_context();
            ctx.add_class("commit-time");
            time
        };

        let author = {
            let author = gtk::Label::new(author);
            author.set_halign(gtk::Align::End);
            author.set_hexpand(true);
            let ctx = author.get_style_context();
            ctx.add_class("commit-authors");
            author
        };
        time_author.add(&time);
        time_author.add(&author);

        let scrolled = {
            let scrolled =
                gtk::ScrolledWindow::new(None::<&gtk::Adjustment>, None::<&gtk::Adjustment>);
            scrolled.set_policy(gtk::PolicyType::Automatic, gtk::PolicyType::Automatic);
            scrolled.add(files_tree.as_ref());
            scrolled
        };

        let view = gtk::Box::new(gtk::Orientation::Vertical, 16);
        view.add(&desc);
        view.add(&time_author);
        view.add(&scrolled);
        if show_changes {
            let legend_view = legend::new();
            view.add(&legend_view);
        }
        view.show_all();

        Revision { view, files_tree }
    }

    /// Append a file to the tree view.
    pub fn append<'a, P: Into<Option<&'a gtk::TreeIter>>>(
        &self,
        parent: P,
        icon: &str,
        name: &str,
    ) -> gtk::TreeIter {
        let tree_iter = self.files_tree.append(parent, icon, name, true);
        self.files_tree.columns_autosize();

        tree_iter
    }
}

impl AsRef<gtk::Box> for Revision {
    #[inline]
    fn as_ref(&self) -> &gtk::Box {
        &self.view
    }
}
