// Copyright 2019 Sam Whited
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use gtk;
use gtk::ContainerExt;
use gtk::InfoBarExt;
use gtk::LabelExt;
use gtk::RevealerExt;
use gtk::WidgetExt;

use glib;
use glib::Cast;

/// Notifier is used to create in-app notifications that don't need to persist after the
/// application is closed, such as error messages.
#[derive(Debug, Clone)]
pub struct Notifier {
    revealer: gtk::Revealer,
    info_bar: gtk::InfoBar,
    label: gtk::Label,
}

impl Notifier {
    /// Creates a new notifier.
    pub fn new() -> Notifier {
        let revealer = gtk::Revealer::new();
        revealer.set_transition_type(gtk::RevealerTransitionType::SlideDown);

        let info_bar = gtk::InfoBar::new();
        info_bar.set_show_close_button(true);
        info_bar.connect_response(clone!( revealer => move |_, _| {
            revealer.set_reveal_child(false);
        }));
        info_bar.connect_close(clone!( revealer => move |_| {
            revealer.set_reveal_child(false);
        }));

        revealer.add(&info_bar);

        let icon =
            gtk::Image::new_from_icon_name("dialog-error", gtk::IconSize::LargeToolbar.into());
        let label = gtk::Label::new("");

        if let Some(area) = info_bar.get_content_area() {
            if let Ok(c) = area.downcast::<gtk::Container>() {
                c.add(&icon);
                c.add(&label);
            }
        }

        revealer.show();
        revealer.show_all();

        Notifier {
            revealer,
            info_bar,
            label,
        }
    }

    /// Shows an in-app notification with an error icon.
    pub fn show_error(&self, msg: &str) {
        self.info_bar.set_message_type(gtk::MessageType::Error);
        self.label.set_text(msg);
        self.revealer.set_reveal_child(true);
    }
}

impl AsRef<gtk::Revealer> for Notifier {
    #[inline]
    fn as_ref(&self) -> &gtk::Revealer {
        &self.revealer
    }
}
