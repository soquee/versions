// Copyright 2019 Sam Whited
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use crate::ui;

use glib;
use glib::ObjectExt;

use gtk;
use gtk::ContainerExt;
use gtk::GtkListStoreExtManual;
use gtk::HeaderBarExt;
use gtk::PanedExt;
use gtk::SizeGroupExt;
use gtk::StackExt;
use gtk::StackSwitcherExt;
use gtk::StyleContextExt;
use gtk::WidgetExt;

/// The main project view.
#[derive(Debug, Clone)]
pub struct Project {
    paned: gtk::Paned,
    sidebar: ui::Sidebar,
    stack: gtk::Stack,
    placeholder: gtk::Image,
    switcher: gtk::StackSwitcher,
    files_container: gtk::Box,
    diff_container: gtk::Box,
    unsaved_changes_buttons: gtk::Box,
    add_files: ui::Add,
    model: gtk::ListStore,
}

impl Project {
    /// Creates a new project view that contains a sidebar list and main pane.
    pub fn new(header_bar: &ui::SplitHeaderBar) -> Self {
        let model = gtk::ListStore::new(&[gtk::Type::String, gtk::Type::String]);
        let hbox = header_bar.get_paned();
        let hbar = header_bar.get_main_bar();
        let shbar = header_bar.get_sidebar();
        let unsaved_changes_buttons = header_bar.get_unsaved_changes_buttons();
        let sidebar = ui::Sidebar::new(&header_bar, &model);

        let diff_container = gtk::Box::new(gtk::Orientation::Horizontal, 16);
        let files_container = gtk::Box::new(gtk::Orientation::Horizontal, 16);

        let stack = {
            let stack = gtk::Stack::new();
            let ctx = stack.get_style_context();
            ctx.add_class("content");
            stack
        };
        let switcher = gtk::StackSwitcher::new();
        stack.add_titled(&files_container, "allfiles", "All Files");
        stack.add_titled(&diff_container, "diffs", "Changes");
        switcher.set_stack(&stack);
        switcher.set_sensitive(false);
        switcher.show();
        stack.show_all();

        hbar.set_custom_title(&switcher);

        let sidebar_group = gtk::SizeGroup::new(gtk::SizeGroupMode::Horizontal);
        sidebar_group.add_widget(shbar);
        sidebar_group.add_widget(sidebar.as_ref());

        let placeholder = ui::new_placeholder();

        let paned = gtk::Paned::new(gtk::Orientation::Horizontal);
        paned
            .bind_property("position", hbox, "position")
            .flags(glib::BindingFlags::BIDIRECTIONAL | glib::BindingFlags::SYNC_CREATE)
            .build();
        sidebar
            .as_ref()
            .bind_property("visible", shbar, "visible")
            .flags(glib::BindingFlags::BIDIRECTIONAL | glib::BindingFlags::SYNC_CREATE)
            .build();
        paned.pack1(sidebar.as_ref(), false, false);
        paned.pack2(&placeholder, true, true);
        paned.show_all();

        let add_files = ui::Add::new();

        Project {
            paned,
            sidebar,
            stack,
            placeholder,
            switcher,
            files_container,
            diff_container,
            unsaved_changes_buttons,
            add_files,
            model,
        }
    }

    /// Set the second pane to contain a placeholder.
    pub fn show_placeholder(&self) {
        self.unsaved_changes_buttons.hide();
        self.switcher.set_sensitive(false);
        if let Some(child) = self.paned.get_child2() {
            self.paned.remove(&child);
        }
        self.paned.add2(&self.placeholder);
    }

    /// Show the stack in the main pane.
    pub fn show_stack(&self) {
        self.unsaved_changes_buttons.hide();
        if let Some(child) = self.paned.get_child2() {
            self.paned.remove(&child);
        }
        self.paned.add2(&self.stack);
        self.switcher.set_sensitive(true);
    }

    /// Show the add files pane.
    pub fn show_add_files(&self, author: &str) {
        self.unsaved_changes_buttons.show();
        self.switcher.set_sensitive(false);
        if let Some(child) = self.paned.get_child2() {
            self.paned.remove(&child);
        }
        self.add_files.set_author(author);
        self.paned.add2(self.add_files.as_ref());
    }

    /// Append files to the add files view.
    pub fn append_add_files<'a, P>(
        &self,
        parent: P,
        icon_name: &str,
        name: &str,
        ignored: bool,
    ) -> gtk::TreeIter
    where
        P: Into<Option<&'a gtk::TreeIter>>,
    {
        self.add_files.append(parent, icon_name, name, ignored)
    }

    /// Clear the add files view.
    pub fn clear_add_files(&self) {
        self.add_files.clear();
    }

    /// Return the selected files from the add files view.
    pub fn selected_files(&self) -> Vec<String> {
        self.add_files.selected_files()
    }

    /// Autosize the add files view columns.
    pub fn autosize_add_files(&self) {
        self.add_files.columns_autosize();
    }

    /// Get the commit message entered into the add files dialog.
    pub fn get_msg(&self) -> String {
        self.add_files.get_msg()
    }

    /// Register a handler to be called when an item is selected in the sidebar.
    ///
    /// One or more signals may be emitted when multiple items are selected, or a signal may be
    /// omitted for a row that is already selected so always check what the selection actually is.
    pub fn connect_selection_changed<F>(&self, f: F) -> glib::SignalHandlerId
    where
        F: Fn(&gtk::TreeSelection) + 'static,
    {
        self.sidebar.connect_selection_changed(f)
    }

    /// Set children in the main pane.
    pub fn set_children<P, Q>(&self, diff: &P, files: &Q)
    where
        P: gtk::IsA<gtk::Widget>,
        Q: gtk::IsA<gtk::Widget>,
    {
        for child in self.files_container.get_children() {
            self.files_container.remove(&child);
        }
        for child in self.diff_container.get_children() {
            self.diff_container.remove(&child);
        }
        self.files_container.add(files);
        self.diff_container.add(diff);
    }

    /// Clear the commit message text fields.
    pub fn clear_msg(&self) {
        self.add_files.clear_msg();
    }

    /// Highlights and selects the summary text entry to warn the user that it is required.
    pub fn warn_no_msg(&self) {
        self.add_files.warn_no_msg();
    }

    /// Unselects all the nodes in the sidebar.
    pub fn sidebar_unselect_all(&self) {
        self.sidebar.unselect_all();
    }

    /// Insert a commit into the sidebar.
    pub fn sidebar_insert<P>(&self, position: P, title: &str, hash: &str)
    where
        P: Into<Option<u32>>,
    {
        self.model
            .insert_with_values(position.into(), &[0, 1], &[&title, &hash]);
    }
}

impl AsRef<gtk::Paned> for Project {
    #[inline]
    fn as_ref(&self) -> &gtk::Paned {
        &self.paned
    }
}
