// Copyright 2019 Sam Whited
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/// Result is a result ype that always uses one of our Error variants.
pub type Result<T> = std::result::Result<T, Error>;

/// Possible error values that can occur when creating applications.
#[derive(Debug)]
pub enum Error {
    Bool(glib::error::BoolError),
    Git(git2::Error),
    Glib(glib::Error),
    Unknown,
}

impl From<glib::error::BoolError> for Error {
    fn from(err: glib::error::BoolError) -> Self {
        Error::Bool(err)
    }
}

impl From<git2::Error> for Error {
    fn from(err: git2::Error) -> Self {
        Error::Git(err)
    }
}

impl From<glib::Error> for Error {
    fn from(err: glib::Error) -> Self {
        Error::Glib(err)
    }
}

impl From<()> for Error {
    fn from(_: ()) -> Self {
        Error::Unknown
    }
}
