## Submitting Patches

To submit a patch, first learn to use `git send-email` by reading
[git-send-email.io], then read the SourceHut [mailing list etiquette] guide.
You can send patches to my general purpose patches [mailing list].

Please prefix the subject with `[PATCH versions]`.
To configure your checkout of this repo to always use the correct prefix and
send to the correct list cd into the repo and run:

    git config sendemail.to ~samwhited/patches@lists.sr.ht
    git config format.subjectPrefix 'PATCH versions'

[git-send-email.io]: https://git-send-email.io/
[mailing list etiquette]: https://man.sr.ht/lists.sr.ht/etiquette.md
[mailing list]: https://lists.sr.ht/~samwhited/patches


## License

Licensed under the [Apache 2.0] license, a copy of which can be found in the
[`LICENSE`] file.

[Apache 2.0]: https://www.apache.org/licenses/LICENSE-2.0
[`LICENSE`]: ./LICENSE


### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you shall be licensed as above, without any
additional terms or conditions.
